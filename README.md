## AIS Restaurant

Semester school project to Advanced Information Systems course at Faculty of Information Technology, Brno University of Technology implementing information system for imaginary restaurant.

#### Authors:

- **Tomáš Líbal** - xlibal00
- **Pavel Eis** - xeispa00
- **Tomáš Chocholatý** - xchoch07
- **Lukáš Ondrák** - xondra49
- **Ondřej Novák** - xnovak2b

#### Technologies
- Java, Spring Boot, Hibernate, Oracle SQL Server

### Prerequisites:

- Install `Java JDK 11`,  `Maven` and some SQL database (implemented and tested on Oracle SQL database)

- In the root of the project repository run `$ mvn clean install -DskipTests`, which will build the app.
- The last thing is to fill database configuration. In the file `src/main/resources/application.properties` fill the following keys:
  - `spring.datasource.url` - URL to database, e.g.  **jdbc:oracle:thin:@//gort.fit.vutbr.cz:1521/orclpdb**
  - `spring.datasource.username` - username of database user
  - `spring.datasource.password` - password of database user

### Running the server

- Application can be started running `$ mvn spring-boot:run`