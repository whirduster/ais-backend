package cz.vutbr.fit.PISBE.configuration;

import cz.vutbr.fit.PISBE.authentication.Jwt;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Auth extends HttpFilter {
    /*
    Spring filter, which tries to parse 'Authorization' header from each request. If it contains header, then
    it sets the token into SecurityContextHolder, does nothing otherwise
    */

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String authorization = request.getHeader("Authorization");
        String bearerType = "Bearer ";
        if (authorization != null && authorization.startsWith(bearerType)) {
            String token = authorization.substring(bearerType.length());
            Principal p = Jwt.verify(token);
            SecurityContextHolder.getContext()
                    .setAuthentication(new UsernamePasswordAuthenticationToken(p, null));
        }

        chain.doFilter(request, response);
    }

    public static Principal me() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (Principal) auth.getPrincipal();
    }
}
