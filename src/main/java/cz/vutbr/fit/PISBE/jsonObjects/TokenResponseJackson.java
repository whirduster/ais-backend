package cz.vutbr.fit.PISBE.jsonObjects;

public class TokenResponseJackson {
    private String token;

    public TokenResponseJackson() {
    }

    public TokenResponseJackson(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
