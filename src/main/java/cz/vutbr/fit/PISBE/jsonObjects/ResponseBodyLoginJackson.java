package cz.vutbr.fit.PISBE.jsonObjects;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ResponseBodyLoginJackson extends ResponseBodyMessageJackson {
    private String token;
    private List<String> roles;

    public ResponseBodyLoginJackson() {
    }

    public ResponseBodyLoginJackson(String token){
        this.token = token;
    }

    public ResponseBodyLoginJackson(String token, List<String> roles, String message) {
        super(message);
        this.token = token;
        this.roles = roles;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
