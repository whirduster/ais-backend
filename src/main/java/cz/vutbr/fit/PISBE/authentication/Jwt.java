package cz.vutbr.fit.PISBE.authentication;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import cz.vutbr.fit.PISBE.configuration.Principal;

import java.util.List;

public class Jwt {
    private static final String SECRET = "PISs off";

    public static String create(String email, List<String> roles) {
        return JWT.create()
                .withClaim("email", email)
                .withClaim("roles", roles)
                .sign(Algorithm.HMAC256(SECRET));
    }

    public static Principal verify(String token) {
        DecodedJWT verify = JWT.require((Algorithm.HMAC256(SECRET)))
                .build().verify(token);

        return new Principal(
                verify.getClaim("email").asString(),
                verify.getClaim("roles").asList(String.class)
        );
    }
}
