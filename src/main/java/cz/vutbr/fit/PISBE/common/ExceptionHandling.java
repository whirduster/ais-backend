package cz.vutbr.fit.PISBE.common;

import org.hibernate.validator.internal.engine.ConstraintViolationImpl;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;

@Service
public class ExceptionHandling {
    public ResponseHolder handleDatabaseException(Exception e){
        Throwable t = e.getCause();
        if(t.getMessage().equals("could not execute statement")) {
            return ResponseHolder.returnBadRequestAndMessage("Tento název již existuje!");
        }
        while ((t != null) && !(t instanceof ConstraintViolationException)) {
            t = t.getCause();
        }
        if (t instanceof ConstraintViolationException) {
            // Here you're sure you have a ConstraintViolationException, you can handle it
            Object[] violations = ((ConstraintViolationException) t).getConstraintViolations().toArray();
            ConstraintViolationImpl violation = (ConstraintViolationImpl)violations[0];
            String violationMessage = violation.getMessage();
            return ResponseHolder.returnBadRequestAndMessage(violationMessage);
        }
        return ResponseHolder.returnBadRequestAndMessage("Nastal problém při přístupu k systémové databázi!");
    }
}
