package cz.vutbr.fit.PISBE.features.menuItem.jsonObjects;

import com.fasterxml.jackson.annotation.JsonInclude;


public class FullMenuItemJackson extends MenuItemJackson {
    private String categoryName;
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private boolean inMenu;
    private double productionPrice;

    public FullMenuItemJackson() {
    }

    public FullMenuItemJackson(String categoryName, boolean inMenu, double productionPrice) {
        this.categoryName = categoryName;
        this.inMenu = inMenu;
        this.productionPrice = productionPrice;
    }

    public FullMenuItemJackson(int menuItemId, String name, double price, String categoryName, boolean inMenu, double productionPrice) {
        super(menuItemId, name, price);
        this.categoryName = categoryName;
        this.inMenu = inMenu;
        this.productionPrice = productionPrice;
    }

    public FullMenuItemJackson(int menuItemId, String name, double price, double productionPrice, String grammage, String description, String categoryName, boolean inMenu) {
        super(menuItemId, name, price, grammage, description);
        this.categoryName = categoryName;
        this.inMenu = inMenu;
        this.productionPrice = productionPrice;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public boolean isInMenu() {
        return inMenu;
    }

    public void setInMenu(boolean inMenu) {
        this.inMenu = inMenu;
    }

    public double getProductionPrice() {
        return productionPrice;
    }

    public void setProductionPrice(double productionPrice) {
        this.productionPrice = productionPrice;
    }
}
