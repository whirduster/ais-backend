package cz.vutbr.fit.PISBE.features.user;

import cz.vutbr.fit.PISBE.features.role.Role;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "Users")
public class User {
    @Id
    @SequenceGenerator(name = "UserIdGenerator", sequenceName = "USER_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UserIdGenerator")
    @Column(name = "userID")
    private int userId;

    @Column(name = "email")
    private String email;

    @Length(min=5, message="Minimální délka hesla je 5 znaků")
    @Column(name = "password")
    private String password;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "surname")
    private String surname;

    @NotNull(message = "Atribut RegisteredUser je povinný!")
    @Column(name = "RegisteredUser")
    private boolean registeredUser;

    @ManyToMany(cascade = CascadeType.REFRESH)
    @JoinTable(name = "Has", joinColumns = @JoinColumn(name = "userId"), inverseJoinColumns = @JoinColumn(name = "roleID"))
    private Set<Role> roles;

    public User() {

    }

    public User(String email, @Length(min = 5, message = "Minimální délka hesla je 5 znaků") String password, String firstName, String surname, boolean registeredUser) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.surname = surname;
        this.registeredUser = registeredUser;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public boolean isRegisteredUser() {
        return registeredUser;
    }

    public void setRegisteredUser(boolean registeredUser) {
        this.registeredUser = registeredUser;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
