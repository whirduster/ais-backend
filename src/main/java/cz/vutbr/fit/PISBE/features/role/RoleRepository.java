package cz.vutbr.fit.PISBE.features.role;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {

    Role findByType(String role);

}
