package cz.vutbr.fit.PISBE.features.reservation;

public enum State {
    APPROVED, DENIED, FINISHED
}
