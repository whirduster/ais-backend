package cz.vutbr.fit.PISBE.features.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.vutbr.fit.PISBE.authentication.Jwt;
import cz.vutbr.fit.PISBE.common.ExceptionHandling;
import cz.vutbr.fit.PISBE.common.JsonToObjectParser;
import cz.vutbr.fit.PISBE.common.MD5;
import cz.vutbr.fit.PISBE.common.ResponseHolder;
import cz.vutbr.fit.PISBE.configuration.Auth;
import cz.vutbr.fit.PISBE.configuration.Principal;
import cz.vutbr.fit.PISBE.dto.RolesList;
import cz.vutbr.fit.PISBE.features.role.Role;
import cz.vutbr.fit.PISBE.features.role.RoleRepository;
import cz.vutbr.fit.PISBE.features.user.jsonObjects.*;
import cz.vutbr.fit.PISBE.jsonObjects.ResponseBodyLoginJackson;
import cz.vutbr.fit.PISBE.jsonObjects.TokenResponseJackson;
import cz.vutbr.fit.PISBE.mail.NotificationService;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.stream.Collectors;

import static cz.vutbr.fit.PISBE.common.JsonToObjectParser.getStringValueFromNode;

//import cz.vutbr.fit.PISBE.common.MailSender;

@Service
public class UserServiceDaoImpl implements UserServiceDao {
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    NotificationService notificationService;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    ObjectFactory<HttpSession> sessionFactory;
    @Autowired
    UserServiceDao userServiceDao;
    @Autowired
    ExceptionHandling exceptionHandling;

    @Override
    public void saveUserWitchEncryptedPassword(User user) {
        user.setPassword(MD5.getMd5(user.getPassword()));
        userRepository.save(user);
    }

    public ArrayList<String> getRolesAsString(Collection<Role> userRoles){
        ArrayList<String> roleList = new ArrayList<>();
        for (Role role : userRoles) {
            roleList.add(role.getType());
        }
        return roleList;
    }

    public Set<String> parseRolesFromStringList(String roles){
        return new HashSet<String>(Arrays.asList(roles.split(",")));
    }

    public boolean canAccess(String rolesWithAuthorization){
        /*
        rolesWithAuthorization is String, which holds all roles, which can access endpoint split by ','
        eg: Chef,Manager,Cook
         */
        Principal me;
        try {
            me = Auth.me();
        }
        catch (NullPointerException e){
            return false;
        }

        List<User> tests = userRepository.findAllByEmail(me.getEmail());
        User currentUser = null;
        for (User test:tests) {
            if(test.isRegisteredUser()) {
                currentUser = test;
            }
        }
        Set<Role> userRoles = currentUser.getRoles();
        Set<String> parsedRolesWithAuthorization = parseRolesFromStringList(rolesWithAuthorization);
        // intersection of rolesWithAuthorization and userRoles and result is saved in rolesWithAuthorization
        parsedRolesWithAuthorization.retainAll(getRolesAsString(userRoles));
        // if role intersection is empty, then user should not have access --> return false
        return !parsedRolesWithAuthorization.isEmpty();
    }

    private ArrayList<AllEmployeesResponseJackson> getAllEmployeesResponseBody(List<User> allEmployees){
        ArrayList<AllEmployeesResponseJackson> allEmployeesResponseBody = new ArrayList<>();
        for (User employee : allEmployees) {
            // iterate through every user (employee), get info about him and transfer all his roles (objects) to ArrayList of Strings
            EmployeeInfo employeeInfo = new EmployeeInfo(employee.getEmail(), employee.getFirstName(), employee.getSurname());
            ArrayList<Role> rolesObjectsList = new ArrayList<>(employee.getRoles());
            ArrayList<String> roleList = getRolesAsString(rolesObjectsList);
            allEmployeesResponseBody.add(new AllEmployeesResponseJackson(employeeInfo, roleList));
        }
        return allEmployeesResponseBody;
    }

    public ResponseHolder getAllEmployees() {
        if (!userServiceDao.canAccess("Manager")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        List<User> allEmployees = userRepository.findAll();
        ArrayList<User> employees = new ArrayList<>();
        for (User person : allEmployees) {
            Set<Role> roles = person.getRoles();
            Role role = roleRepository.findByType("Logged user");
            if((!roles.contains(role))&&(person.isRegisteredUser())){
                if(person.getRoles().size() != 0) {
                    if(!person.getEmail().equals("admin@restaurant.cz")) {
                        employees.add(person);
                    }
                }
            }
        }

        ArrayList<AllEmployeesResponseJackson> responseJson = getAllEmployeesResponseBody(employees);

        ResponseHolder<ArrayList<AllEmployeesResponseJackson>> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(responseJson);

        return responseHolder;
    }

    private List<String> getUsersRoles(User user){
        return user.getRoles().stream()
                .map(Role::getType)
                .collect(Collectors.toList());
    }

    private String getNewToken(User user){
        return Jwt.create(user.getEmail(), getUsersRoles(user));
    }

    public ResponseHolder loginUserFromJson(String json) {
        BasicUserJackson basicUserJackson;
        try {
            basicUserJackson = JsonToObjectParser.parseJsonToObject(json, BasicUserJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseHolder.returnBadRequestAndMessage("Některé údaje nebyly správně vyplněny.");
        }

         User user = userRepository.findByEmail(basicUserJackson.getEmail());
        if (user != null) {
            if (user.getPassword().equals(basicUserJackson.getPassword())) {
                String token = getNewToken(user);
                return ResponseHolder.loginResponse(HttpServletResponse.SC_OK, token, getUsersRoles(user), "Uživatel byl přihlášen.");
            } else {
                return ResponseHolder.returnBadRequestAndMessage("Chybně zadané heslo.");
            }

        } else {
            return ResponseHolder.returnBadRequestAndMessage("Uživatel nebyl nalezen.");
        }
    }

    public ResponseHolder createNewUserFromJson(String json) {
        UserJackson userJackson;
        try {
            userJackson = JsonToObjectParser.parseJsonToObject(json, UserJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseHolder.returnBadRequestAndMessage("Některé údaje nebyly správně vyplněny.");
        }


        List<User> tests = userRepository.findAllByEmail(userJackson.getEmail());
        boolean exist = false;
        for (User test: tests) {
            if (test.isRegisteredUser()) {
                exist = true;
                break;
            }
        }
        if (!exist) {
            User user = new User(userJackson.getEmail(), userJackson.getPassword(), userJackson.getName(), userJackson.getSurname(), true);

            Role userRole = roleRepository.findByType("Logged user");
            user.setRoles(new HashSet<>(Collections.singletonList(userRole)));
            try {
                userRepository.save(user);
                ResponseHolder<TokenResponseJackson> responseHolder = new ResponseHolder<>();
                responseHolder.setReturnCode(HttpServletResponse.SC_CREATED);
                responseHolder.setResponseBody(new TokenResponseJackson(getNewToken(user)));
                return responseHolder;
            } catch (Exception e) {
                return exceptionHandling.handleDatabaseException(e);
            }
        } else {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_CONFLICT, "Chyba, uživatel s tímto emailem už je registrován.");
        }
    }


    public ResponseHolder addNewEmployeeFromJson(String json) {
        if (!userServiceDao.canAccess("Manager")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        EmployeeJackson employeeJackson;
        try {
            employeeJackson = JsonToObjectParser.parseJsonToObject(json, EmployeeJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseHolder.returnBadRequestAndMessage("Některé údaje nebyly správně vyplněny.");
        }
        if ((userRepository.findByEmail(employeeJackson.getEmployeeInfo().getEmail()) == null)) {
            String password = new Random().ints(10, 48, 122).mapToObj(i -> String.valueOf((char) i)).collect(Collectors.joining());

            User user = new User(employeeJackson.getEmployeeInfo().getEmail(), password, employeeJackson.getEmployeeInfo().getName(), employeeJackson.getEmployeeInfo().getSurname(), true);

            try {
                notificationService.sendNotification(user);
            } catch (MailException e) {
                System.out.println("error mail");
            }

            user.setPassword(MD5.getMd5(user.getPassword()));
            ArrayList<RolesList> rolesList = employeeJackson.getRoles();
            List<Role> roles = new ArrayList<Role>();
            for (RolesList entry : rolesList) {
                if (entry.getHasRole()) {
                    roles.add(roleRepository.findByType(entry.getName()));
                }
            }

            user.setRoles(new HashSet<>(roles));
            try {
                userRepository.save(user);
            } catch (Exception e) {
                return exceptionHandling.handleDatabaseException(e);
            }
        } else {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_CONFLICT, "Chyba, zaměstnanec s tímto emailem už je registrován.");
        }
        return ResponseHolder.returnOkAndMessage("Zaměstnanec byl úspěšně vytvořen.");
    }

    public ResponseHolder editEmployee(String json, String email) throws JsonProcessingException {
        if (!userServiceDao.canAccess("Manager")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        if (email.equals("admin@restaurant.cz")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Tento účet nelze editovat. Pro editaci tohoto účtu kontrolujte prosím technickou podporu.");
        }

        String attribute = getStringValueFromNode(json, "attribute");
        String value = getStringValueFromNode(json, "value");

        User employee = userRepository.findByEmail(email);
        if (attribute.equals("email")) {
            if (userRepository.findByEmail(value) == null) {
                employee.setEmail(value);
            }else{
                return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_CONFLICT, "Chyba, zaměstnanec s tímto emailem už je registrován.");
            }
        } else if (attribute.equals("name")) {
            employee.setFirstName(value);
        } else if (attribute.equals("surname")) {
            employee.setSurname(value);
        }

        try {
            userRepository.save(employee);
        } catch (Exception e) {
            return exceptionHandling.handleDatabaseException(e);
        }
        return ResponseHolder.returnOkAndMessage("Editace proběhla korektně.");
    }

    public ResponseHolder editEmployeesRole(String json, String email) throws JsonProcessingException {
        if (!userServiceDao.canAccess("Manager")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        if (email.equals("admin@restaurant.cz")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Tento účet nelze editovat. Pro editaci tohoto účtu kontrolujte prosím technickou podporu.");
        }

        String roleForEdit = getStringValueFromNode(json, "roleName");
        if(roleForEdit == null){
            return ResponseHolder.returnBadRequestAndMessage("Nebyla zadána žádná role pro editaci.");
        }

        User employee = userRepository.findByEmail(email);
        if(employee == null){
            return ResponseHolder.returnBadRequestAndMessage("Uživatel s tímto emailem neexistuje.");
        }

        Set<Role> roles = employee.getRoles();
        boolean userHasRole = false;
        for (Role role : roles) {
            if(role.getType().equals(roleForEdit)) {
                employee.getRoles().remove(role);
                userHasRole = true;
                userRepository.save(employee);
                break;
            }
        }

        if (!userHasRole){
            roles.add(roleRepository.findByType(roleForEdit));
            employee.setRoles(new HashSet<>(roles));
            userRepository.save(employee);
        }

        return ResponseHolder.returnOkAndMessage("Editace role proběhla úspěšně");
    }

    public ResponseHolder deleteEmployee(String email) {
        if (!userServiceDao.canAccess("Manager")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        Principal me = Auth.me();
        String actualUserEmail = me.getEmail();

        if (email.equals(actualUserEmail)) {
            return ResponseHolder.returnBadRequestAndMessage("Nelze smazat vlastní účet.");
        }

        User employee = userRepository.findByEmail(email);
        if (employee == null) {
            return ResponseHolder.returnBadRequestAndMessage("Uživatel s tímto emailem nebyl nalezen.");
        } else {
            userRepository.delete(employee);
            return ResponseHolder.returnOkAndMessage("Uživatel byl úspěšně vymazán.");
        }
    }

    public ResponseHolder getUserInfo() {
        if (!userServiceDao.canAccess("Waiter,Logged user,Chef,Manager,Cook")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        Principal me = Auth.me();
        String actualUserEmail = me.getEmail();


        List<User> tests = userRepository.findAllByEmail(me.getEmail());
        User user = null;
        for (User test:tests) {
            if(test.isRegisteredUser()) {
                user = test;
            }
        }
        HostInfoJackson responseJson = new HostInfoJackson(user.getEmail(), user.getFirstName(), user.getSurname());

        ResponseHolder<HostInfoJackson> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(responseJson);

        return responseHolder;

    }

    public ResponseHolder editUserAccount(String requestBody, String email){
        if (!userServiceDao.canAccess("Waiter,Logged user,Chef,Manager,Cook")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        if (email.equals("admin@restaurant.cz")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Tento účet nelze editovat. Pro editaci tohoto účtu kontrolujte prosím technickou podporu.");
        }
        Principal me = Auth.me();
        String actualUserEmail = me.getEmail();

        EditUserInfoJackson editUserInfoJackson;
        try {
            editUserInfoJackson = JsonToObjectParser.parseJsonToObject(requestBody, EditUserInfoJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseHolder.returnBadRequestAndMessage("Některé údaje nebyly správně vyplněny.");
        }



        List<User> tests = userRepository.findAllByEmail(editUserInfoJackson.getEmail());
        boolean exist = false;
        User tempUser = null;
        for (User test: tests) {
            if (test.isRegisteredUser()) {
                exist = true;
                tempUser = test;
                break;
            }
        }

        if (exist&&(!actualUserEmail.equals(editUserInfoJackson.getEmail()))) {
            return ResponseHolder.returnBadRequestAndMessage("Uživatel s tímto emailem již existuje.");
        }

        if(tempUser == null){
            return ResponseHolder.returnBadRequestAndMessage("Uživatel s tímto emailem neexistuje.");
        }

        tempUser.setEmail(editUserInfoJackson.getEmail());
        tempUser.setFirstName(editUserInfoJackson.getName());
        tempUser.setSurname(editUserInfoJackson.getSurname());

        if((editUserInfoJackson.getPassword() != null) && (editUserInfoJackson.getOldPassword() != null)) {
            if(editUserInfoJackson.getOldPassword().equals(tempUser.getPassword())){
                tempUser.setPassword(editUserInfoJackson.getPassword());
            }else{
                return ResponseHolder.returnBadRequestAndMessage("Aktuální heslo se neshoduje.");
            }
        }
        try {
            userRepository.save(tempUser);
        } catch (Exception e) {
            return exceptionHandling.handleDatabaseException(e);
        }

        String token = getNewToken(tempUser);

        ResponseHolder<ResponseBodyLoginJackson> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(new ResponseBodyLoginJackson(token));

        return responseHolder;
    }

}
