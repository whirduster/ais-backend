package cz.vutbr.fit.PISBE.features.user.jsonObjects;

public class UserJackson extends BasicUserJackson {
    private String name;
    private String surname;

    public UserJackson() {
    }

    public UserJackson(String email, String password, String name, String surname) {
        super(email, password);
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
