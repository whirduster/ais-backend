package cz.vutbr.fit.PISBE.features.orderItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
public class OrderItemController {
    @Autowired
    OrderItemServiceDao orderItemServiceDao;

    @GetMapping(value = "/order_items")
    public Object getAllOrderItems(HttpServletResponse response){
        return orderItemServiceDao.getAllOrderItems().setup(response);
    }

    @PostMapping(value = "/places/{placeId:[\\d]+}/add_order_item/{menuItemId:[\\d]+}")
    public Object createNewOrderItem(HttpServletResponse response, @PathVariable int placeId, @RequestBody String requestBody,
                                     @PathVariable int menuItemId){
        return orderItemServiceDao.createNewOrderItem(placeId, menuItemId, requestBody).setup(response);
    }

    @PostMapping(value = "/places/{placeId:[\\d]+}/order_items/{orderItemId:[\\d]+}/requirement")
    public Object addSpecialRequirement(HttpServletResponse response, @PathVariable int placeId, @RequestBody String requestBody,
                                     @PathVariable int orderItemId){
        return orderItemServiceDao.addSpecialRequirement(placeId, orderItemId, requestBody).setup(response);
    }

    @DeleteMapping(value = "/places/{placeId:[\\d]+}/order_items/{orderItemId:[\\d]+}")
    public Object deleteOrderItem(HttpServletResponse response, @PathVariable int placeId, @PathVariable int orderItemId){
        return orderItemServiceDao.deleteOrderItem(placeId, orderItemId).setup(response);
    }

    @PutMapping(value = "/order_items/{orderItemId:[\\d]+}/preparing")
    public Object markOrderItemAsPreparing(HttpServletResponse response, @PathVariable int orderItemId){
        return orderItemServiceDao.markOrderItemAsPreparing(orderItemId).setup(response);
    }

    @PutMapping(value = "/order_items/{orderItemId:[\\d]+}/finished")
    public Object markOrderItemAsFinished(HttpServletResponse response, @PathVariable int orderItemId){
        return orderItemServiceDao.markOrderItemAsFinished(orderItemId).setup(response);
    }

    @PutMapping(value = "/order_items/{orderItemId:[\\d]+}/released")
    public Object markOrderItemAsReleased(HttpServletResponse response, @PathVariable int orderItemId){
        return orderItemServiceDao.markOrderItemAsReleased(orderItemId).setup(response);
    }


    @GetMapping(value = "/places/{placeId:[\\d]+}/add_order_item")
    public Object getOrderItemForm(HttpServletResponse response, @PathVariable int placeId){
        return orderItemServiceDao.getOrderItemForm(placeId).setup(response);
    }
}
