package cz.vutbr.fit.PISBE.features.orderItem.jsonObjects;

import cz.vutbr.fit.PISBE.features.category.jsonObjects.ShowMenuJackson;
import cz.vutbr.fit.PISBE.features.category.jsonObjects.ShowOrderItemsJackson;

import java.util.ArrayList;

public class GetOrderItemForm {
    private ArrayList<ShowOrderItemsJackson> categoriesOrderItems;
    private ArrayList<ShowMenuJackson> categoriesMenuItems;

    public GetOrderItemForm() {
    }

    public GetOrderItemForm(ArrayList<ShowOrderItemsJackson> categoriesOrderItems, ArrayList<ShowMenuJackson> categoriesMenuItems) {
        this.categoriesOrderItems = categoriesOrderItems;
        this.categoriesMenuItems = categoriesMenuItems;
    }

    public ArrayList<ShowOrderItemsJackson> getCategoriesOrderItems() {
        return categoriesOrderItems;
    }

    public void setCategoriesOrderItems(ArrayList<ShowOrderItemsJackson> categoriesOrderItems) {
        this.categoriesOrderItems = categoriesOrderItems;
    }

    public ArrayList<ShowMenuJackson> getCategoriesMenuItems() {
        return categoriesMenuItems;
    }

    public void setCategoriesMenuItems(ArrayList<ShowMenuJackson> categoriesMenuItems) {
        this.categoriesMenuItems = categoriesMenuItems;
    }
}
