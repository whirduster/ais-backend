package cz.vutbr.fit.PISBE.features.menuItem.jsonObjects;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class MenuItemJackson {
    private int menuItemId;
    private String name;
    private double price;
    private String grammage;
    private String description;

    public MenuItemJackson() {
    }

    public MenuItemJackson(int menuItemId, String name, double price) {
        this.menuItemId = menuItemId;
        this.name = name;
        this.price = price;
    }

    public MenuItemJackson(int menuItemId, String name, double price, String grammage, String description) {
        this.menuItemId = menuItemId;
        this.name = name;
        this.price = price;
        this.grammage = grammage;
        this.description = description;
    }

    public int getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(int menuItemId) {
        this.menuItemId = menuItemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getGrammage() {
        return grammage;
    }

    public void setGrammage(String grammage) {
        this.grammage = grammage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
