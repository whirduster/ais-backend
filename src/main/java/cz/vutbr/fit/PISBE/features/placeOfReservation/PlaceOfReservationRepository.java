package cz.vutbr.fit.PISBE.features.placeOfReservation;

import cz.vutbr.fit.PISBE.features.reservation.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PlaceOfReservationRepository extends JpaRepository<PlaceOfReservation, Integer> {

//    @Query("select POR.PLACEID from RESERVATION JOIN PLACE_OF_RESERVATION POR ON RESERVATION.PLACE_OF_RESERVATION_REFERENCE = POR.PLACEID where RESERVATION.START_OF_RESERVATION_DATE BETWEEN TO_DATE('2020-12-13 01:00:00', 'yyyy-MM-dd HH24:mi:ss') and TO_DATE('2020-12-13 04:00:00', 'yyyy-MM-dd HH24:mi:ss') or       RESERVATION.END_OF_RESERVATION_DATE BETWEEN TO_DATE('2020-12-13 01:00:00', 'yyyy-MM-dd HH24:mi:ss') and TO_DATE('2020-12-13 04:00:00', 'yyyy-MM-dd HH24:mi:ss') group by POR.PLACEID")

//
//    @Query("select POR.PLACEID from RESERVATION JOIN PLACE_OF_RESERVATION POR ON RESERVATION.PLACE_OF_RESERVATION_REFERENCE = POR.PLACEID where RESERVATION.START_OF_RESERVATION_DATE BETWEEN ?1 and ?2 or RESERVATION.END_OF_RESERVATION_DATE BETWEEN ?1 and ?2 group by POR.PLACEID")
//    List<PlaceOfReservation> getAllReservationsByTableInTime(Date startOfReservation, Date endOfReservation);

//    @Query("select por.idPlace from Reservation res join PlaceOfReservation por on res. = por.placeId where reservation.startOfReservationDate between ?1 and ?2 or reservation.endOfReservationDate between ?1 and ?2 group by por.placeId")
//    List<PlaceOfReservation> getAllReservationsByTableInTime(Date startDate, Date endDate);
    PlaceOfReservation findByPlaceID(int placeId);
}
