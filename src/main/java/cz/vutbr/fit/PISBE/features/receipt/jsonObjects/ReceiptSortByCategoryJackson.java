package cz.vutbr.fit.PISBE.features.receipt.jsonObjects;

import java.util.ArrayList;

public class ReceiptSortByCategoryJackson {
    private String categoryName;
    private ArrayList<ReceiptJackson> receiptsItems;

    public ReceiptSortByCategoryJackson() {
    }

    public ReceiptSortByCategoryJackson(String categoryName, ArrayList<ReceiptJackson> receiptsItems) {
        this.categoryName = categoryName;
        this.receiptsItems = receiptsItems;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public ArrayList<ReceiptJackson> getReceiptsItems() {
        return receiptsItems;
    }

    public void setReceiptsItems(ArrayList<ReceiptJackson> receiptsItems) {
        this.receiptsItems = receiptsItems;
    }
}

