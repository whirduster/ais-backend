package cz.vutbr.fit.PISBE.features.reservation;

import cz.vutbr.fit.PISBE.features.placeOfReservation.PlaceOfReservation;
import cz.vutbr.fit.PISBE.features.user.User;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "Reservation")
public class Reservation {
    @Id
    @NotNull(message = "ID rezervace je povinné")
    @SequenceGenerator(name = "ReservationIdGenerator", sequenceName = "RESERVATION_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ReservationIdGenerator")
    @Column(name = "reservationID")
    private int reservationId;

    @NotNull(message = "Stav rezervace nebyl zadán!")
    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private State state;

    @NotNull(message = "Očekávaný počet hostů nebyl zadán!")
    @Column(name = "expectedNumberOfGuests")
    private int expectedNumberOfPeople;

    @NotNull(message = "Čas rezervace nebyl zadán!")
    @Column(name = "startOfReservationDate")
    private Date reservationDate;

    @NotNull(message = "Očekávaný konec rezervace nebyl zadán!")
    @Column(name = "endOfReservationDate")
    private Date endOfReservationDate;

    @NotNull(message = "Kód pro smazání rezervace nebyl zadán!")
    @Column(name = "codeForDelete")
    private String codeForDelete;

    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "userReference", referencedColumnName = "userId")
    private User userReference;

    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "placeOfReservationReference", referencedColumnName = "placeID")
    private PlaceOfReservation placeOfReservationReference;

    public Reservation() {
    }


    public Reservation(State state, int expectedNumberOfPeople, Date reservationDate, Date endOfReservationDate, String codeForDelete, User userReference, PlaceOfReservation placeOfReservationReference) {
        this.state = state;
        this.expectedNumberOfPeople = expectedNumberOfPeople;
        this.reservationDate = reservationDate;
        this.endOfReservationDate = endOfReservationDate;
        this.codeForDelete = codeForDelete;
        this.userReference = userReference;
        this.placeOfReservationReference = placeOfReservationReference;
    }

    public int getReservationId() {
        return reservationId;
    }

    public void setReservationId(int reservationId) {
        this.reservationId = reservationId;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getExpectedNumberOfPeople() {
        return expectedNumberOfPeople;
    }

    public void setExpectedNumberOfPeople(int expectedNumberOfPeople) {
        this.expectedNumberOfPeople = expectedNumberOfPeople;
    }

    public Date getReservationDate() {
        return reservationDate;
    }

    public String getReservationDateFormatted(){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.reservationDate);
    }

    public void setReservationDate(Date reservationDate) {
        this.reservationDate = reservationDate;
    }

    public String getCodeForDelete() {
        return codeForDelete;
    }

    public void setCodeForDelete(String codeForDelete) {
        this.codeForDelete = codeForDelete;
    }

    public Date getEndOfReservationDate() {
        return endOfReservationDate;
    }

    public String getEndOfReservationDateFormated(){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.endOfReservationDate);
    }

    public void setEndOfReservationDate(Date endOfReservationDate) {
        this.endOfReservationDate = endOfReservationDate;
    }

    public User getUserReference() {
        return userReference;
    }

    public void setUserReference(User userReference) {
        this.userReference = userReference;
    }

    public PlaceOfReservation getPlaceOfReservationReference() {
        return placeOfReservationReference;
    }

    public void setPlaceOfReservationReference(PlaceOfReservation placeOfReservationReference) {
        this.placeOfReservationReference = placeOfReservationReference;
    }
}
