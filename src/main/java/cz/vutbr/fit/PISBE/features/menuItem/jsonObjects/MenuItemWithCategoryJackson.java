package cz.vutbr.fit.PISBE.features.menuItem.jsonObjects;

import cz.vutbr.fit.PISBE.features.category.Category;

import java.util.List;

public class MenuItemWithCategoryJackson {
    private String categoryName;
    private List<MenuItemJackson> menuItems;

    public MenuItemWithCategoryJackson() {
    }

    public MenuItemWithCategoryJackson(String categoryName, List<MenuItemJackson> menuItems) {
        this.categoryName = categoryName;
        this.menuItems = menuItems;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<MenuItemJackson> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItemJackson> menuItems) {
        this.menuItems = menuItems;
    }
}
