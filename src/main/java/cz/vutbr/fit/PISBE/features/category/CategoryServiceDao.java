package cz.vutbr.fit.PISBE.features.category;

import cz.vutbr.fit.PISBE.common.ResponseHolder;
import cz.vutbr.fit.PISBE.features.category.jsonObjects.GetMenuDetailsJackson;
import cz.vutbr.fit.PISBE.features.category.jsonObjects.ShowMenuJackson;

import java.util.ArrayList;

public interface CategoryServiceDao {
    ResponseHolder<ArrayList<ShowMenuJackson>> getMenu();

    ResponseHolder<GetMenuDetailsJackson> getMenuDetails();

    ResponseHolder deleteCategory(String categoryId);

    ResponseHolder createNewCategory(String requestBody);

    ResponseHolder getAllCategories();
}
