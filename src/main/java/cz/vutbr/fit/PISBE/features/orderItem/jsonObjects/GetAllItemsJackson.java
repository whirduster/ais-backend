package cz.vutbr.fit.PISBE.features.orderItem.jsonObjects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class GetAllItemsJackson {
    private int orderItemId;
    private int menuItemId;
    private int placeId;
    private String placeType;
    private String createdAt;
    private String name;
    private String specialRequirements;
    private String state;
    private String grammage;

    public GetAllItemsJackson(){}

    public GetAllItemsJackson(int menuItemId, String specialRequirements){
        this.menuItemId = menuItemId;
        this.specialRequirements = specialRequirements;
    }

    public GetAllItemsJackson(int orderItemId, int menuItemId, int placeId, String placeType, String createdAt, String name,
                              String specialRequirements, String state, String grammage) {
        this.orderItemId = orderItemId;
        this.menuItemId = menuItemId;
        this.placeId = placeId;
        this.placeType = placeType;
        this.createdAt = createdAt;
        this.name = name;
        this.specialRequirements = specialRequirements;
        this.state = state;
        this.grammage = grammage;
    }

    public int getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(int orderItemId) {
        this.orderItemId = orderItemId;
    }

    public int getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(int menuItemId) {
        this.menuItemId = menuItemId;
    }

    public int getPlaceId() {
        return placeId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    public String getPlaceType() {
        return placeType;
    }

    public void setPlaceType(String placeType) {
        this.placeType = placeType;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialRequirements() {
        return specialRequirements;
    }

    public void setSpecialRequirements(String specialRequirements) {
        this.specialRequirements = specialRequirements;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getGrammage() {
        return grammage;
    }

    public void setGrammage(String grammage) {
        this.grammage = grammage;
    }
}
