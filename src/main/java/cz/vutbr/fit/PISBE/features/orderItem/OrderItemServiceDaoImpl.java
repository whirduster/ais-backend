package cz.vutbr.fit.PISBE.features.orderItem;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.vutbr.fit.PISBE.common.ExceptionHandling;
import cz.vutbr.fit.PISBE.common.JsonToObjectParser;
import cz.vutbr.fit.PISBE.common.ResponseHolder;
import cz.vutbr.fit.PISBE.features.category.Category;
import cz.vutbr.fit.PISBE.features.category.CategoryRepository;
import cz.vutbr.fit.PISBE.features.category.CategoryServiceDaoImpl;
import cz.vutbr.fit.PISBE.features.category.jsonObjects.ShowMenuJackson;
import cz.vutbr.fit.PISBE.features.category.jsonObjects.ShowOrderItemsJackson;
import cz.vutbr.fit.PISBE.features.menuItem.MenuItem;
import cz.vutbr.fit.PISBE.features.menuItem.MenuItemRepository;
import cz.vutbr.fit.PISBE.features.orderItem.jsonObjects.GetAllItemsJackson;
import cz.vutbr.fit.PISBE.features.orderItem.jsonObjects.GetOrderItemForm;
import cz.vutbr.fit.PISBE.features.orderItem.jsonObjects.OrderItemJackson;
import cz.vutbr.fit.PISBE.features.placeOfReservation.PlaceOfReservation;
import cz.vutbr.fit.PISBE.features.placeOfReservation.PlaceOfReservationRepository;
import cz.vutbr.fit.PISBE.features.user.UserServiceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class OrderItemServiceDaoImpl implements OrderItemServiceDao {
    @Autowired
    UserServiceDao userServiceDao;

    @Autowired
    OrderItemRepository orderItemRepository;

    @Autowired
    PlaceOfReservationRepository placeOfReservationRepository;

    @Autowired
    MenuItemRepository menuItemRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    CategoryServiceDaoImpl categoryServiceDao;

    @Autowired
    ExceptionHandling exceptionHandling;

    public Date getDateFromString(String date) throws ParseException{
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date);
    }

    public ResponseHolder getAllOrderItems() {
        if (!userServiceDao.canAccess("Cook,Chef,Waiter")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }
        List<OrderItem> allOrderItems = orderItemRepository.findAllOrOrderByCreatedAt();
        Map<MenuItem, ArrayList<GetAllItemsJackson>> orderItemsSorted = new HashMap<>();

        ArrayList<GetAllItemsJackson> allOrderItemsResponse = new ArrayList<>();

        // go through every OrderItem and add them to list of corresponding MenuItem, they should remain sorted by created at
        for (OrderItem orderItem : allOrderItems) {
            MenuItem menuItem = orderItem.getMenuItemReference();
            PlaceOfReservation placeOfReservation = orderItem.getPlaceOfReservationReference();
            GetAllItemsJackson getAllItemsJackson = new GetAllItemsJackson(orderItem.getOrderItemId(), menuItem.getMenuItemId(),
                    placeOfReservation.getPlaceID(), placeOfReservation.getType().name(), orderItem.getCreatedAtFormatted(),
                    menuItem.getName(), orderItem.getSpecialRequirement(), orderItem.getState().name(), menuItem.getGrammage());
            try {
                orderItemsSorted.get(menuItem).add(getAllItemsJackson);
            }
            catch (NullPointerException e){
                orderItemsSorted.put(menuItem, new ArrayList<>());
                orderItemsSorted.get(menuItem).add(getAllItemsJackson);
            }
        }

        // find menuItem, which has youngest OrderItem and tak its arrayList of OrderItems and add it to final
        // response, then delete it from Map and repeat
        while (!orderItemsSorted.isEmpty()) {
            ArrayList<GetAllItemsJackson> youngest = null;
            MenuItem youngestRef = null;
            for (Map.Entry<MenuItem, ArrayList<GetAllItemsJackson>> entry : orderItemsSorted.entrySet()) {
                try {
                    if (getDateFromString(youngest.get(0).getCreatedAt()).compareTo(getDateFromString(entry.getValue().get(0).getCreatedAt())) > 0) {
                        youngest = entry.getValue();
                        youngestRef = entry.getKey();
                    }
                } catch (NullPointerException e) {
                    youngest = entry.getValue();
                    youngestRef = entry.getKey();
                } catch (ParseException e) {
                    continue;
                }
            }
            allOrderItemsResponse.addAll(youngest);
            orderItemsSorted.remove(youngestRef);
        }

        ResponseHolder<ArrayList<GetAllItemsJackson>> responseHolder = new ResponseHolder();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(allOrderItemsResponse);
        return responseHolder;
    }

    public ResponseHolder createNewOrderItem(int placeId, int menuItemId, String requestBody) {
        if (!userServiceDao.canAccess("Waiter")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        OrderItem newOrderItem = new OrderItem("", State.WAITING, new Timestamp(System.currentTimeMillis()));
        newOrderItem.setPlaceOfReservationReference(placeOfReservationRepository.findByPlaceID(placeId));
        newOrderItem.setMenuItemReference(menuItemRepository.findByMenuItemId(menuItemId));
        try {
            orderItemRepository.save(newOrderItem);
        } catch (Exception e) {
            return exceptionHandling.handleDatabaseException(e);
        }

        ResponseHolder responseHolder = new ResponseHolder();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(new OrderItemJackson(newOrderItem.getOrderItemId()));
        return responseHolder;
    }

    public ResponseHolder addSpecialRequirement(int placeId, int orderItemId, String requestBody){
        if (!userServiceDao.canAccess("Waiter")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        GetAllItemsJackson orderItemInfo;
        try {
            orderItemInfo = JsonToObjectParser.parseJsonToObject(requestBody, GetAllItemsJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseHolder.returnBadRequestAndMessage("Některé údaje nebyly správně vyplněny.");
        }

        OrderItem orderItem;
        try {
            orderItem = orderItemRepository.findByOrderItemId(orderItemId);
        }
        catch (Exception e){
            return ResponseHolder.returnBadRequestAndMessage("Položka objednávky s tímto ID neexistuje!");
        }

        try {
            orderItem.setSpecialRequirement(orderItemInfo.getSpecialRequirements());
            orderItemRepository.save(orderItem);
        }
        catch (Exception e){
            return exceptionHandling.handleDatabaseException(e);
        }

        return ResponseHolder.returnEmptyBody(HttpServletResponse.SC_OK);
    }

    public ResponseHolder deleteOrderItem(int placeId, int orderItemId) {
        if (!userServiceDao.canAccess("Waiter")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        OrderItem orderItem = orderItemRepository.findByOrderItemId(orderItemId);
        if (orderItem == null) {
            return ResponseHolder.returnBadRequestAndMessage("Objednaná položka s tímto ID nebyla nalezena.");
        }
        else {
            orderItemRepository.delete(orderItem);
            return ResponseHolder.returnOkAndMessage("Objednaná položka byla úspěšně vymazána.");
        }
    }

    private ResponseHolder changeOrderItemState(int orderItemId, State newState){
        OrderItem orderItem = orderItemRepository.findByOrderItemId(orderItemId);
        if (orderItem == null) {
            return ResponseHolder.returnBadRequestAndMessage("Objednaná položka s tímto ID nebyla nalezena.");
        }
        else {
            orderItem.setState(newState);
            orderItemRepository.save(orderItem);
            return ResponseHolder.returnEmptyBody(HttpServletResponse.SC_OK);
        }
    }

    public ResponseHolder markOrderItemAsPreparing(int orderItemId) {
        if (!userServiceDao.canAccess("Cook,Chef")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        return changeOrderItemState(orderItemId, State.PREPARING);
    }

    public ResponseHolder markOrderItemAsFinished(int orderItemId){
        if (!userServiceDao.canAccess("Cook,Chef")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        return changeOrderItemState(orderItemId, State.FINISHED);
    }

    public ResponseHolder markOrderItemAsReleased(int orderItemId){
        if (!userServiceDao.canAccess("Waiter")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        return changeOrderItemState(orderItemId, State.RELEASED);
    }

    public ResponseHolder getOrderItemForm(int placeId){
        if (!userServiceDao.canAccess("Waiter")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        List<Category> categories = categoryRepository.findAll();
        ArrayList<ShowMenuJackson> categoriesMenuItems;
        ArrayList<ShowOrderItemsJackson> categoriesOrderItems;
        try {
            categoriesMenuItems = categoryServiceDao.getAllCategoriesWithMenuItems(categories);
            categoriesOrderItems = categoryServiceDao.getAllOrderItemsAtPlace(categories, placeId);
        }
        catch (Exception e){
            return exceptionHandling.handleDatabaseException(e);
        }

        GetOrderItemForm response = new GetOrderItemForm(categoriesOrderItems, categoriesMenuItems);

        ResponseHolder responseHolder = new ResponseHolder();
        responseHolder.setResponseBody(response);
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        return responseHolder;
    }
}
