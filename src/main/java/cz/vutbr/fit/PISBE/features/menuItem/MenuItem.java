package cz.vutbr.fit.PISBE.features.menuItem;

import cz.vutbr.fit.PISBE.features.category.Category;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "MenuItem")
public class MenuItem {
    @Id
    @NotNull(message = "ID položky menu je povinné")
    @SequenceGenerator(name = "MenuItemIdGenerator", sequenceName = "MENU_ITEM_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MenuItemIdGenerator")
    @Column(name = "menuItemID")
    private int menuItemId;

    @NotNull(message = "Cena nebyla zadána!")
    @Column(name = "price")
    private double price;

    @NotNull(message = "Výrobní cena nebyla zadána!")
    @Column(name = "productionPrice")
    private double productionPrice;

    @NotNull(message = "Název poožky menu nebyl zadán!")
    @Column(name = "name")
    @Size(max = 30, message = "Název položky menu může mít maximálně 30 znaků!")
    private String name;

    @Column(name = "grammage")
    @NotNull(message = "Nebyla zadána hmotnost!")
    @Size(max = 15, message = "Hmotnost položky může mít maximálně 15 znaků!")
    private String grammage;

    @Column(name = "description")
    @Size(max=200, message = "Popis může mít maximálně 200 znaků.")
    private String description;

    @NotNull(message = "Nebylo zadáno, zda je položka v menu!")
    @Column(name = "inMenu")
    private boolean inMenu;

    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "categoryReference", referencedColumnName = "categoryId")
    private Category categoryReference;

    public MenuItem() {
    }

    public MenuItem(double price, double productionPrice, String name, String grammage, String description, boolean inMenu) {
        this.price = price;
        this.productionPrice = productionPrice;
        this.name = name;
        this.grammage = grammage;
        this.description = description;
        this.inMenu = inMenu;
    }

    public int getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(int menuItemId) {
        this.menuItemId = menuItemId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getProductionPrice() {
        return productionPrice;
    }

    public void setProductionPrice(double productionPrice) {
        this.productionPrice = productionPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGrammage() { return grammage; }

    public void setGrammage(String grammage) { this.grammage = grammage; }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isInMenu() {
        return inMenu;
    }

    public void setInMenu(boolean inMenu) {
        this.inMenu = inMenu;
    }

    public Category getCategoryReference() {
        return categoryReference;
    }

    public void setCategoryReference(Category categoryReference) {
        this.categoryReference = categoryReference;
    }
}
