package cz.vutbr.fit.PISBE.features.menuItem;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
public class MenuItemController {
    @Autowired
    MenuItemServiceDao menuItemServiceDao;

    @GetMapping(value = "/menu_items")
    public Object getAllMenuItems(HttpServletResponse response) {
        return menuItemServiceDao.getAllMenuItems().setup(response);
    }

    @GetMapping(value = "/menu_items/new")
    public Object getAllCategoriesForMenuItemForm(HttpServletResponse response) {
        return menuItemServiceDao.getAllCategoriesForMenuItemForm().setup(response);
    }

    @PostMapping(value = "/menu_items/new")
    public Object createNewMenuItem(HttpServletResponse response, @RequestBody String requestBody) {
        return menuItemServiceDao.createNewMenuItem(requestBody).setup(response);
    }

    @GetMapping(value = "menu_items/{menuItemId:[\\d]+}")
    public Object getMenuItem(HttpServletResponse response, @PathVariable int menuItemId) {
        return menuItemServiceDao.getMenuItem(menuItemId).setup(response);
    }

    @PutMapping(value = "menu_items/{menuItemId:[\\d]+}")
    public Object editMenuItem(HttpServletResponse response, @RequestBody String requestBody, @PathVariable int menuItemId) {
        return menuItemServiceDao.editMenuItem(requestBody, menuItemId).setup(response);
    }

    @DeleteMapping(value = "/menu_items/{menuItemId:[\\d]+}")
    public Object deleteMenuItem(HttpServletResponse response, @PathVariable int menuItemId) {
        return menuItemServiceDao.deleteMenuItem(menuItemId).setup(response);
    }

    @PostMapping(value = "/menu/edit")
    public Object editMenuPost(HttpServletResponse response, @RequestBody String requestBody) {
        return menuItemServiceDao.editMenuPost(requestBody).setup(response);
    }

    @GetMapping(value = "/sales/count")
    public Object getSalesOfMenuItems(HttpServletResponse response){
        return menuItemServiceDao.getSalesOfMenuItems().setup(response);
    }

    @GetMapping(value = "sales/{year:[\\d]+}/{month:[\\d]+}")
    public Object getSalesForPeriod(HttpServletResponse response, @PathVariable int year,  @PathVariable int month){
        return menuItemServiceDao.getSalesForPeriod(year, month).setup(response);
    }

    @GetMapping(value = "sales/get_average_utilization")
    public Object getUtilization(HttpServletResponse response){
        return menuItemServiceDao.getUtilization().setup(response);
    }

    @GetMapping(value = "/sales/timeline")
    public Object getTimesOfSale(HttpServletResponse response){
        return menuItemServiceDao.getTimesOfSale().setup(response);
    }

    @GetMapping(value = "/sales/last")
    public Object getLastTimeSold(HttpServletResponse response){
        return menuItemServiceDao.getLastTimeSold().setup(response);
    }
}
