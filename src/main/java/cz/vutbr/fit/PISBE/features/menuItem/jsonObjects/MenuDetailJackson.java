package cz.vutbr.fit.PISBE.features.menuItem.jsonObjects;

import java.util.ArrayList;

public class MenuDetailJackson {
    private ArrayList<MenuItemWithCategoryJackson> categoriesInMenu;
    private ArrayList<MenuItemWithCategoryJackson> categories;


    public MenuDetailJackson() {
    }

    public MenuDetailJackson(ArrayList<MenuItemWithCategoryJackson> categoriesInMenu, ArrayList<MenuItemWithCategoryJackson> categories) {
        this.categoriesInMenu = categoriesInMenu;
        this.categories = categories;
    }

    public ArrayList<MenuItemWithCategoryJackson> getCategoriesInMenu() {
        return categoriesInMenu;
    }

    public void setCategoriesInMenu(ArrayList<MenuItemWithCategoryJackson> categoriesInMenu) {
        this.categoriesInMenu = categoriesInMenu;
    }

    public ArrayList<MenuItemWithCategoryJackson> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<MenuItemWithCategoryJackson> categories) {
        this.categories = categories;
    }
}
