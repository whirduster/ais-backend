package cz.vutbr.fit.PISBE.features.user.jsonObjects;

public class EditUserInfoJackson extends HostInfoJackson {
    private String oldPassword;
    private String password;

    public EditUserInfoJackson() {
    }

    public EditUserInfoJackson(String oldPassword, String password) {
        this.oldPassword = oldPassword;
        this.password = password;
    }

    public EditUserInfoJackson(String email, String name, String surname, String oldPassword, String password) {
        super(email, name, surname);
        this.oldPassword = oldPassword;
        this.password = password;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
